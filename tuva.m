function stats = tuva( X, varargin )
% stats = tuva ( X, ... )
% 
% This function computes my 'total unique variation' analysis between the
% columns of X (converted to a real-valued double matrix). X is assumed to
% be a tall (samples x channels) or (samples x dimensions) matrix.
% 
% This analysis aims to estimate the 'total independent information' in a
% high-dimensional matrix of data
% 
% Typical usage:
%  stats = tuva ( data_matrix, analysis_options{:}, '-plot' )
% 
% Options:
% -xmin [1e-9] : rows of X where no value is greater than xmin are 
%                excluded from analysis
% -canon       : compute linear relations using canoncorr 
%                (default: matrix left-division) 
% -q           : quiet mode
% -z           : z-score columns of X before computation (good for waves)
% -a           : perform sequencing based on |r| (good for waves, default
%                if any x<0)
% -plot        : generate output graphic, default if nargout = 0. 
% -details     : plot instead a full figure detailing solution internals
% -replot [s]  : replot supplied object (do not recompute)
% -labels {''} : use supplied electrode labels 
% -seq [...]   : add channels in specified sequence (override this logic)
% 
% Demo options (1D profile demo is run if nargin = 0)
% -demo          : make demo
% -demo-n [24]   : number of random-walk processes
% -demo-a [0.01] : amplitude of random walk
% -demo-c [...]  : vector of gauss centers for demo
% -demo-s [1]    : scalar or vector of gauss widths (sigma) for demo
% 
% Plot options
% -no-normalise : plot without normalisation (i.e. raw RMS values)
% -no-marginal  : exclude marginal 
% -median-rms   : normalise by median RMS (default: mean) 
% 
% ------------------------------------------------------------------------
% COPYRIGHT 2023 Synchron, Inc.
% 
% Licensed under the MIT License (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%  https://opensource.org/licenses/MIT
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% ------------------------------------------------------------------------
% 
% v1.0 (6 January 2023) ceiber@synchron.com


if nargin > 0, varargin = [{X} varargin]; end % syntax for X-ignored
named = @(v) strncmpi(v,varargin,length(v)); % name,value syntax
get_  = @(v) varargin{find(named(v))+1};    

if nargin == 0 || any(named('-demo')), stats = demo_mode(varargin{:}); return, end
if any(named('-replot'))
    if isstruct(X), plot_summary(X)
    else            plot_summary(get_('-replot'))
    end
    return 
end

X = double(real( X ));
nD = size(X, 2); 

opts.xmin = 1e-9; 
opts.use_cca    = any(named('-canon')); 
opts.use_zscore = any(named('-z'));
if opts.use_zscore, X = zscore(X); end
opts.use_abs_r  = any(named('-a')) ~= any(X(:) < 0);
opts.use_mi     = any(named('-mutual'));
opts.plus_c     = any(named('-plus'));
opts.set_seq    = any(named('-seq'));
opts.verbose    = ~any(named('-q'));

do_details_figure = any(named('-details'));

if any(named('-xmin')), opts.xmin = get_('-xmin'); end


% Initialise with minimum-correlation pair
ok = all(~isnan(X), 2) & any(abs(X) > opts.xmin, 2);

if opts.use_mi

    r_map = zeros(size(X,2));

    for a = 1:size(X,2)
     for b = 1:size(X,2)
       % www.mathworks.com/matlabcentral/fileexchange/14888-mutual-information-computation
       r_map(a,b) = mutualinfo(X(ok,a),X(ok,b));
     end
    end

    if all(r_map(:) == 0), 
        error('failed to apply mutual information analysis')
    end

else
    r_map = corr( X(ok,:) ); 
end

if opts.use_abs_r, abs_ = @abs; else abs_ = @(x) x; end

if opts.set_seq
    cca_squence = 'auto';
    try cca_sequence = get_('-seq');
    catch err, cca_squence = 'auto'; %#ok<NASGU> 
    end
    if ischar(cca_squence), cca_sequence = 1:size(X,2); end    
    c1 = cca_sequence(1);
    c2 = cca_sequence(2); 
else   
    [~,idx] = min(abs_(r_map(:)),[],'omitnan');
    [c1,c2] = ind2sub(size(r_map), idx);
    cca_sequence = sort([c1 c2]); 
    c1 = cca_sequence(1);
    c2 = cca_sequence(2);
end

%% Graphical output of initial input
if do_details_figure, 
    %%
    subplot(2,3,1), 
    imagesc(abs_(r_map)), caxis([min([0 caxis]) 1])
    % if opts.use_abs_r, caxis([0 1]), else caxis([-1 1]), end
    axis image off, hold on
    plot(c1,c2,'s','Color',[.5 .5 .5]), 
    ylabel(colorbar,'r_{init}')
end



%% Sequential Canonical Correlation Analysis
    
rms_ = @(x) sqrt(mean(x.^2)); 
cca_r = {r_map};

const = ones(size(X(ok,1))); 
if ~opts.plus_c, const = []; end

if opts.use_cca
     [w,v,~] = canoncorr(X(ok,c1), X(ok,c2)); 
else  w = [X(ok,c1) const] \ X(ok,c2); v = 1;
end

cca_rms = [rms_(X(ok,[c1 c1 c2])) ... 
           rms_(X(ok,c2) - [X(ok,c1) const]*(w/v) ) ];
cca_rms = reshape(cca_rms,2,2);

if opts.set_seq, cca_todo = cca_sequence(3:end);
else             cca_todo = setdiff(1:nD, cca_sequence); 
end


if do_details_figure, 

    if any(named('-la')), labels = get_('-la');
    else labels = arrayfun(@(e) sprintf('E%d', e), 1:size(X,2),'unif',0);
    end

    subplot(1,3,[2 3])
    plot_detail_set([], labels, const) % reset and pass in fixed values
    plot_detail_set(X(ok,:), [], r_map, sort([c2 cca_todo]), c1)
end



if opts.verbose, printInfo(), end

while ~isempty(cca_todo)

    r_iter = 0*cca_todo;  
    w_pred = [];

    for ee = cca_todo

        if opts.set_seq && ee ~= cca_todo(1), 
            r_iter(2:end) = []; 
            break, 
        end

        cca_inc = setdiff(cca_sequence,cca_todo); % if -seq supplied
        
        if opts.verbose
            printInfo('Iter %d, E=%d ... ', numel(cca_sequence)-1, ee)
        end
        
        if opts.use_cca
           [w,v,r] = canoncorr(X(ok,cca_sequence), X(ok,ee)); 
        else
            v = 1;
            w = [X(ok,cca_inc) const] \ X(ok,ee); 
            x_pred = [X(ok,cca_inc) const] * w; 
            r = corr(x_pred, X(ok,ee));
        end

        if opts.use_mi
            if opts.use_cca 
                error todo_test_combination_of_opts
                x_pred = [X(ok,cca_sequence) const] * (w/v);
            end
            r = mutualinfo(x_pred, X(ok,ee)); 
        end
    
        r_iter(cca_todo == ee) = r; 
        w_pred = [w_pred w/v]; %#ok<AGROW> 
    end

    [~,sel] = min(abs_(r_iter)); 
    ee = cca_todo(sel); 

    if do_details_figure, 
        plot_detail_set(X(ok,:), w_pred, r_iter, cca_todo, cca_inc )
    end

    cca_r{1,end+1} = r_iter; %#ok<AGROW> 

    x_pred = [X(ok,cca_inc) const] * w_pred(:, sel); 

    cca_rms = [cca_rms [rms_(X(ok,ee)); ...
                        rms_(X(ok,ee) - x_pred)]]; %#ok<AGROW> 
    
    if ~opts.set_seq, cca_sequence = [cca_sequence ee]; %#ok<AGROW> 
    end
    cca_todo(sel) = []; 
end
if opts.verbose, printInfo(), fprintf('Done.\n'),  end
stats.sequence = cca_sequence; 
stats.rms_component = cca_rms(1,:);
stats.rms_margainal = cca_rms(2,:);
stats.rms_total = cumsum(cca_rms(2,:));
if opts.use_mi
     stats.mutual_information = cca_r;
else stats.correlations = cca_r;
end
stats.options = opts;

%%
if ~any(named('-p')) && nargout > 0, return, end

if do_details_figure, subplot(2,3,4), end

plot_summary(stats)

if nargout == 0, clear, end
return


%% Plot visualisation
function plot_summary(stats)

get_ = evalin('caller','get_');
named = evalin('caller','named'); 

cla reset

ee = 1:max(stats.sequence); 

C = lines(7); G = @(v) [v v v]/10; 

px = [ee; ee; nan*ee]; 
py = stats.rms_total - [0;1;1] * stats.rms_component;

if any(named('-median-rms')), avg_ = @median; else avg_ = @mean; end

if any(named('-no-n')), norm_value = 1;
else  norm_value = avg_(stats.rms_component);    
end,  py = py / norm_value; 

plot(px(:), py(:), '-', 'LineWidth', 1.3, 'Color', G(3)), hold on

if ~any(named('-no-m')) % no-marginal
    pm = stats.rms_margainal / norm_value; 
    plot(ee, pm, '.r-', 'lineWidth', 1.1, 'Color', C(3,:),'markersize',12)
end

plot(ee, py(1,:), '-o', 'lineWidth', 1.3, 'Color', C(1,:), 'markerfacecolor','w')

try tools.tidyPlot, end %#ok<TRYNC> 

if any(named('-la')), labels = get_('-la');
                      labels = labels(stats.sequence);
else labels = arrayfun(@(e) sprintf('E%d', e), stats.sequence,'unif',0);
end

% normalise value 
if any(named('-no-n')), yt = avg_(stats.rms_component);
else yt = 1; 
end

set(gca,'XTick',ee,'XTickLabel', labels)
ylim(ylim)
plot([xlim nan xlim nan ee([1 end])], ...
     [0 0 nan yt yt nan ee([1 end])*yt],'Color',[0 0 0 0.3])
if ~any(named('-no-n'))
    text(ee(end)+0.5, py(end-2), sprintf('%0.2f x', py(end-2)),'Color',C(1,:))
else
    text(ee(end)+0.5, yt, sprintf('%g', yt),'Color',[.7 .7 .7])
    text(ee(end)+0.5, py(end-2), sprintf('%g', py(end-2)),'Color',C(1,:))    
end

%% Plot details of computation
function plot_detail_set(X, w_pred, r_iter, e_todo, e_done)

persistent x_min x_max y_index color const

if isempty(X), x_min = []; 
    labels = w_pred; %#ok<NASGU> 
    const = r_iter;    
    return, 
end

if isempty(x_min), 

    y_index = e_todo;

    c_min = min([0; r_iter(:)]);
    cx = linspace(c_min, 1, 128); 
    gs = abs(cx * 0.5);
    
    color = flipud(turbo(128) .* [1 .9 1]) ;
    color = (color + gs') ./ (1.0 + gs');
    colormap(color)

    color = @(r) interp1(cx', color, r,'nearest','extrap');  %' 
    x_min = min(X(:));
    x_max = max(abs(X));
end

if all(size(r_iter) > 1), r_iter = r_iter(e_todo,e_done); end

if isempty(w_pred), 
    x_pred =  X(:, e_done) ./ x_max(e_done);
    x_id = 0*e_todo + 1; 
else
    x_pred = [X(:,e_done) const] * w_pred;
    x_pred = x_pred ./ x_max(e_todo);
    x_id = 1:numel(e_todo);
end

if isempty(get(gca,'Children')), x0 = 0;
else x0 = max(xlim);
end

if x_min > 0, s_ = @(x) 0.90*x + 0.05; tx = [1 0 0]; ty = [0 0 1];
else          s_ = @(x) 0.45*x + 0.50; tx = [-1 1 nan 0 0]; 
                                       ty = [0 0 nan -1 1];
end

%%
axis image off, xlim([0 x0+1]), ylim([0 numel(y_index)]), hold on

for ee = e_todo

    y0 = find(y_index == ee);
    px = s_(x_pred(:,x_id(e_todo == ee))) + x0;    
    py = s_(X(:,ee) ./ x_max(ee)) + y0 - 1; 
    pc = color(r_iter(e_todo == ee)); 

    plot(s_(tx)+x0, s_(ty)+y0-1, 'Color', [.4 .4 .4],'LineWidth',1.1 )
    plot(px,py,'.','Color',pc)
    if true
      
      w  = [px-x0 ones(size(px))] \ py;
      yf = [s_(tx(1:2)); 1 1]' * w; %'
      xf =  s_(tx(1:2));
      plot(xf+x0,yf, '-','Color', pc,'linewidth',1.1)
    end
end

%% Make example figures 
function stats = demo_mode(varargin)

named = @(v) strncmpi(v,varargin,length(v)); % name,value syntax
get_  = @(v) varargin{find(named(v))+1};    

varargin(named('-demo')) = []; 

do_details_figure = any(named('-details'));
if do_details_figure, close all, end

gaussian = @(x,a,b) 1/b/sqrt(2*pi) * exp( -(x-a).^2/(2*b^2) ); 
x = (-5:0.01:15);
% x = linspace(-5,15,1001); 

c_ = @(x) [x{:}]; 

centroids = [0 2.^(-1:3)];

% centroids = sort([-4:2:4 1 0.5]); 
sigma     = 1;
if any(named('-demo-c')), centroids = get_('-demo-c'); end
if any(named('-demo-s')), sigma = get_('-demo-s'); end

test_data = c_(arrayfun(@(u) gaussian(x',u,sigma), centroids ,'unif',0)); %'

clf
subplot(2,2,1)
plot(x, test_data), title('sensors')
try tools.tidyPlot, end %#ok<TRYNC> 
xlim(x([1 end]))

%% Run correlation analysis on sensitivities 

if do_details_figure, 
    f = gcf;     
    figure, stats = tuva(test_data,varargin{:},'-plot');     
    figure(f), subplot(2,2,2), tuva(stats,'-replot')
else
    subplot(2,2,2)
    stats = tuva(test_data,varargin{:},'-plot'); 
end
    
disp(stats), title('TUVA of sensors')

%% Construct demo test wave data 

wave = 0*test_data; 
sensor = @(x) sum(c_(arrayfun(@(p) gaussian(x,p,1)', centroids,'unif',0))); %'

if any(named('-perlin'))
    %%
    a_space = 0.05;
    a_time  = 0.05; 
    w_term  = 0.25;
    n_time = size(test_data,1);
    n_space = size(test_data,1);

    if any(named('-demo-a')), a_space = get_('-demo-a'); end
    if any(named('-demo-t')), a_time  = get_('-demo-t'); end
    if any(named('-demo-n')), n_time  = get_('-demo-n'); end
    if any(named('-demo-w')), w_term  = get_('-demo-w'); end
    if any(named('-demo-p')), p_terms = get_('-demo-p'); 
        if numel(p_terms) >= 1, a_space = p_terms(1); end
        if numel(p_terms) >= 2, a_time  = p_terms(2); end
        if numel(p_terms) >= 3, w_term  = p_terms(3); end
    end

    [gx,gt] = meshgrid(linspace(1,-1,size(test_data,1)), ...
                       linspace(1,-1,n_time));
    
    gx = sqrt( (gx*(n_space/n_time)).^2 + (gt*(a_space/a_time)).^2 - ... 
                2*gx.*gt.*w_term*(n_space/n_time)*(a_space/a_time));

    gx = 1./fftshift(fftshift(gx,1),2); 
    gx(~isfinite(gx)) = 0;

    perlin_spectrum = ( gx.^(1./a_space) ) .* exp(2i*pi*rand(size(gx))); 
    perlin_spectrum(1) = 0;
    perlin_spectrum(~isfinite(perlin_spectrum)) = 0;

    perlin_sources = real(ifft2(perlin_spectrum,'symmetric'));

    perlin_sources = perlin_sources - mean(perlin_sources,2); % charge balance

    % imagesc(perlin_sources), axis image

    wave = perlin_sources * test_data;

    if ~any(named('-no-heat'))
        subplot(2,2,1), hold on, ylim(ylim)
        imagesc(x, -ylim/2, perlin_sources,'clipping','off')
        set(gca,'layer','top')
    end

else % Recording of collection of random walk


    n_demo = 24; 
    rw_scale = 0.01; 
    if any(named('-demo-n')), n_demo = get_('-demo-n'); end
    if any(named('-demo-a')), rw_scale = get_('-demo-a'); end
    rw_wrap = any(named('-mod'));

    rw_x = 0*test_data(:,1) * ones(1, n_demo); 

    % rw_input = rand(1,n_demo) * range(x) + min(x); 
    rw_input = linspace(x(1),x(end),n_demo+2) + randn(1,n_demo+2)*sigma;
    rw_input([1 end]) = []; 
    
    
    for tt = 1:size(wave,1)
        rw_input = rw_input + randn(size(rw_input)) * rw_scale; 
        if rw_wrap, rw_input = mod(rw_input-min(x),range(x)) + min(x); end
        wave(tt,:) = sensor(rw_input);
        rw_x(tt,:) = rw_input; 
    end
    
    if ~any(named('-no-traj'))
        subplot(2,2,1), hold on, ylim(ylim)
        for nn = 1:n_demo
            plot(rw_x(:,nn), (1:tt)/tt * max(ylim),'Color',[.8 .8 .8 .4])
        end
    end
end

%%
nw = size(wave,2);
% dz = mean(range(wave));

subplot(2,2,3), plot(x, zscore(wave)/3 + (1:nw) ), title('signals')
try tools.tidyPlot, end %#ok<TRYNC> 
set(gca,'YTick',1:nw,'YGrid','on')

if do_details_figure, 
    f = gcf;     
    figure, stats(2) = tuva(wave,varargin{:},'-plot','-z');     
    figure(f), subplot(2,2,4), tuva(stats(2),'-replot')
else
    subplot(2,2,4)
    stats(2) = tuva(wave,varargin{:},'-plot','-z'); 
end
disp(stats(2)), title('TUVA of signals')

%% Quality-of-Life functions copied from my UTILS folder
function printInfo(varargin)
% as fprintf() but erases the previous output first

persistent s;
if nargin == 0, s = ''; return, end
fprintf('%s',char(8*ones(size(s)))); 
s = sprintf(varargin{:});
fprintf('%s',s)





