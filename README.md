# TUVA analysis

This repository contains a Matlab implementation of the 'Total Unique Variation Analysis' (TUVA) algorithm, as presented at [EMBC 2023](https://embc.embs.org/2023/). 

This algorithm computes the cumulative total unique variation between the columns of an input data matrix, which we'll call `X`, which is a tall (samples x channels) or (samples x dimensions) matrix of real-valued measurements. 

The total unique variation (TUV) is a simple and useful measure of total predicted or observed cross-talk in a multi-channel recording environment. 

# TUVA code interface: 

`stats = tuva ( X, ... )`
 
This function computes my 'total unique variation' analysis between the columns of `X`, which is a tall (samples x channels) or (samples x dimensions) matrix of real-valued measurements. This is computed in a step-wise manner using sequential multi-linear regresion.

This analysis aims to estimate the 'total independent information' in a high-dimensional matrix of data, where 'information' is assessed using the variance (or more accurately the RMS) of the input signal. This code also contains visualisation routines to display the cumulative total unique variation and the computed optimal sequence.

Analysis options can be specified using a key-value syntax (similar to command line flags and arguments), as described below:

## Typical usage:
`stats = tuva ( data_matrix, analysis_options{:}, '-plot' )`
 
## Analysis Options:
 
| Input          | Description |
| -------------- | ----------- | 
| `-xmin [1e-9]` | rows of X where no value is greater than xmin are excluded from analysis |
| `-canon`       | compute linear relations using canoncorr (default: matrix left-division) |
| `-q`           | quiet mode  |
| `-z`           | z-score columns of X before computation (good for waves) |
| `-a`           | perform sequencing based on abs(r) (good for waves, default if any x<0) |
| `-plot`        | generate output graphic, default if nargout = 0 |
| `-details`     | plot instead a full figure detailing solution internals (e.g. figure 1) |
| `-replot [s]`  | replot supplied object (do not recompute). `tuva(stats, '-replot')` is also valid. |
| `-labels {''}` | use supplied electrode labels |
| `-seq [...]`   | add channels in specified sequence (override this logic) |

## Demo options (1D profile demo is run if nargin = 0)

| Input            | Description |
| ---------------- | ----------- | 
| `-demo`          | make demo   |
| `-demo-n [24]`   | number of random-walk processes |
| `-demo-a [0.01]` | amplitude of random walk |
| `-demo-c [...]`  | vector of gauss centers for demo |
| `-demo-s [1]`    | scalar or vector of gauss widths (sigma) for demo |
| `-perlin`        | use perlin (dense source model) instead of random-walk (sparse source model) |

## Plot options 

| Input           | Description |
| --------------- | ----------- | 
| `-no-normalise` | plot without normalisation (i.e. raw RMS values) |
| `-no-marginal`  | exclude marginal TUV metric curve (yellow curve) |
| `-median-rms`   | normalise by median RMS (default: mean)          |

## Version Info
v1.0 (6 January 2023) - Calvin Eiber <ceiber@synchron.com>
